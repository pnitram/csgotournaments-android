package com.pnitram.csgotournaments;

import android.content.Context;
import android.databinding.DataBindingUtil;
import android.os.Parcelable;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.RadioButton;
import android.widget.TextView;
import android.widget.Toast;

import com.pnitram.csgotournaments.databinding.RecyclerviewItemBinding;

import org.w3c.dom.Text;

import java.util.ArrayList;
import java.util.Collection;
import java.util.Collections;
import java.util.List;

/**
 * Created by Martin-PC on 02-Jul-16.
 */
public class RecyclerViewAdapter extends RecyclerView.Adapter<RecyclerViewAdapter.MyViewHolder> {

    private LayoutInflater inflater;
    ArrayList<GameModel> items = new ArrayList<>();

    public RecyclerViewAdapter(Context context, ArrayList<GameModel> items) {
        inflater = LayoutInflater.from(context);
        this.items = items;
    }

    @Override
    public MyViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        RecyclerviewItemBinding binding = DataBindingUtil.inflate(inflater, R.layout.recyclerview_item, parent, false);
        return new MyViewHolder(binding);
    }

    @Override
    public void onBindViewHolder(MyViewHolder holder, int position) {
        GameModel item = items.get(position);
        holder.binding.setGamemodel(item);
        holder.binding.executePendingBindings();
    }

    @Override
    public int getItemCount() {
        return items.size();
    }

    public void setPrediction(int groupId, int teamId) {
        for(int i = 0; i < getItemCount(); i++) {
            GameModel gm = items.get(i);
            if(gm != null) {
                if(gm.getGroupId() == groupId) {
                    gm.getTeamA().picked = gm.getTeamA().id == teamId;
                    gm.getTeamB().picked = gm.getTeamB().id == teamId;
                }
            }
        }
        notifyDataSetChanged();
    }

    class MyViewHolder extends RecyclerView.ViewHolder {
        final RecyclerviewItemBinding binding;

        MyViewHolder(RecyclerviewItemBinding binding){
            super(binding.getRoot());
            this.binding = binding;
        }
    }
}
