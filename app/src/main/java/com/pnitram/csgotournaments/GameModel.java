package com.pnitram.csgotournaments;

import android.os.Parcel;
import android.os.Parcelable;

/**
 * Created by Martin-PC on 02-Jul-16.
 */
public class GameModel implements Parcelable{

    private boolean isMatchOver;
    private String match;

    private Team teamA;
    private Team teamB;

    private int sectionId;
    private int groupId;

    public GameModel() {

    }

    public void setTeamA(Team teamA) {
        this.teamA = teamA;
    }

    public void setTeamB(Team teamB) {
        this.teamB = teamB;
    }

    public Team getTeamA() {
        return teamA;
    }

    public Team getTeamB() {
        return teamB;
    }

    public void setMatchOver(boolean matchOver) {
        isMatchOver = matchOver;
    }

    public void setMatch(String match) {
        this.match = match;
    }

    public void setSectionId(int sectionId) {
        this.sectionId = sectionId;
    }

    public void setGroupId(int groupId) {
        this.groupId = groupId;
    }


    public boolean isMatchOver() {
        return isMatchOver;
    }

    public String getMatch() {
        return this.match;
    }

    public int getSectionId() {
        return sectionId;
    }

    public int getGroupId() {
        return groupId;
    }

    protected GameModel(Parcel in) {
        match = in.readString();
        sectionId = in.readInt();
        groupId = in.readInt();
    }

    public static final Creator<GameModel> CREATOR = new Creator<GameModel>() {
        @Override
        public GameModel createFromParcel(Parcel in) {
            return new GameModel(in);
        }

        @Override
        public GameModel[] newArray(int size) {
            return new GameModel[size];
        }
    };

    @Override
    public int describeContents() {
        return 0;
    }

    @Override
    public void writeToParcel(Parcel parcel, int i) {
        parcel.writeString(match);
        parcel.writeInt(sectionId);
        parcel.writeInt(groupId);
    }
}
