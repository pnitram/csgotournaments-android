package com.pnitram.csgotournaments;

import android.content.DialogInterface;
import android.content.Intent;
import android.content.SharedPreferences;
import android.os.AsyncTask;
import android.os.Parcel;
import android.preference.Preference;
import android.preference.PreferenceManager;
import android.support.design.widget.TabLayout;
import android.support.design.widget.FloatingActionButton;
import android.support.design.widget.Snackbar;
import android.support.v4.os.AsyncTaskCompat;
import android.support.v4.view.PagerTabStrip;
import android.support.v7.app.AlertDialog;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.support.v7.widget.Toolbar;

import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentPagerAdapter;
import android.support.v4.view.ViewPager;
import android.os.Bundle;
import android.text.Html;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;

import android.widget.ArrayAdapter;
import android.widget.LinearLayout;
import android.widget.ListView;
import android.widget.RadioButton;
import android.widget.RadioGroup;
import android.widget.RelativeLayout;
import android.widget.TextView;
import android.widget.Toast;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.io.BufferedReader;
import java.io.DataOutput;
import java.io.DataOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.net.HttpURLConnection;
import java.net.MalformedURLException;
import java.net.URL;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collections;
import java.util.HashMap;
import java.util.List;

import javax.net.ssl.HttpsURLConnection;

public class MainActivity extends AppCompatActivity {

    /**
     * The {@link android.support.v4.view.PagerAdapter} that will provide
     * fragments for each of the sections. We use a
     * {@link FragmentPagerAdapter} derivative, which will keep every
     * loaded fragment in memory. If this becomes too memory intensive, it
     * may be best to switch to a
     * {@link android.support.v4.app.FragmentStatePagerAdapter}.
     */
    private SectionsPagerAdapter mSectionsPagerAdapter;

    /**
     * The {@link ViewPager} that will host the section contents.
     */
    private ViewPager mViewPager;
    private TabLayout tabLayout;
    private JSONArray myPredictions;

    private String API_KEY = "C8C55EB28A37C9BCBC4764C01F318399";
    private String EVENT_ID;
    private String STEAMID_KEY;
    private String STEAMID;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        Toolbar toolbar = (Toolbar) findViewById(R.id.toolbar);
        tabLayout = (TabLayout) findViewById(R.id.tabs);

        setSupportActionBar(toolbar);
        // Create the adapter that will return a fragment for each of the three
        // primary sections of the activity.
        mSectionsPagerAdapter = new SectionsPagerAdapter(getSupportFragmentManager());

        // Set up the ViewPager with the sections adapter.
        mViewPager = (ViewPager) findViewById(R.id.container);
        mViewPager.setOffscreenPageLimit(8);

        loadPreferences();
    }

    @Override
    protected void onResume() {
        super.onResume();
        loadPreferences();
    }

    private void loadPreferences() {
        SharedPreferences sharedPreferences = PreferenceManager.getDefaultSharedPreferences(this);

        String eventId = sharedPreferences.getString("event_id", null);
        String steamId = sharedPreferences.getString("steam_id", null);
        String steamApiKey = sharedPreferences.getString("steam_api_key", null);
        String steamidkey_cologne16 = sharedPreferences.getString("steam_id_key_cologne16", null);
        String steamidkey_columbus16 = sharedPreferences.getString("steam_id_key_columbus16", null);

        if(eventId != null && steamId != null && steamApiKey != null && steamidkey_cologne16 != null && steamidkey_columbus16 != null) {
            EVENT_ID = eventId;
            STEAMID = steamId;
            switch (EVENT_ID) {
                case "9":
                    STEAMID_KEY = steamidkey_columbus16;
                    break;
                case "10":
                    STEAMID_KEY = steamidkey_cologne16;
                    break;
            }
            new getTournamentLayout().execute(EVENT_ID, API_KEY, STEAMID_KEY);
        } else {
            Toast.makeText(getApplicationContext(), "Go to Settings and fill out the data", Toast.LENGTH_LONG).show();
        }
    }

    public void setPick(View view) {
        final RadioButton radioButton = (RadioButton)view;
        final RadioGroup radioGroup = (RadioGroup)radioButton.getParent();

        GameModel pick = (GameModel)view.getTag();

        String teamName = radioButton.getText().toString();
        final int teamId;
        final int sectionId = pick.getSectionId();
        final int groupId = pick.getGroupId();
        final int index = radioGroup.indexOfChild(radioButton);

        if(teamName.equals(pick.getTeamA().name)){
            teamId = pick.getTeamA().id;
        } else if(teamName.equals(pick.getTeamB().name)) {
            teamId = pick.getTeamB().id;
        } else {
            return;
        }

        Toast.makeText(getApplicationContext(), String.format("sectionid: %s\ngroupid: %s\nteamid: %s\nindex: %s", sectionId, groupId, teamId, index), Toast.LENGTH_LONG).show();

        if(teamName.equals("TBD")) {
            radioGroup.clearCheck();
            Toast.makeText(getApplicationContext(), "Can't select this", Toast.LENGTH_SHORT).show();
            return;
        }

        final AlertDialog.Builder b = new AlertDialog.Builder(this);
        b.setTitle("Confirm Pick");
        b.setMessage(String.format("Confirm [%s] as your pick to win this matchup", teamName));
        b.setCancelable(true);

        b.setPositiveButton("Make Pick", new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialogInterface, int i) {
                new postPick().execute(String.valueOf(sectionId), String.valueOf(groupId), String.valueOf(index), String.valueOf(teamId), String.valueOf("itemidhere"));
                dialogInterface.dismiss();
            }
        });
        b.setNegativeButton("Cancel", new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialog, int which) {
                dialog.cancel();
            }
        });

        b.setOnCancelListener(new DialogInterface.OnCancelListener() {
            @Override
            public void onCancel(DialogInterface dialogInterface) {
                radioGroup.clearCheck();
            }
        });

        final AlertDialog dialog = b.create();
        dialog.show();
    }

    private void setMyPredictions(JSONArray picks) {
        for(int j = 0; j < mSectionsPagerAdapter.getCount(); j++) {
            ExampleFragment f = (ExampleFragment)mSectionsPagerAdapter.getItem(j);
            for(int i = 0; i < picks.length(); i++) {
                try {
                    int groupId = picks.getJSONObject(i).getInt("groupid");
                    int teamId = picks.getJSONObject(i).getInt("pick");

                    try {
                        f.adapter.setPrediction(groupId, teamId);
                    } catch (Exception e) {
                        e.printStackTrace();
                    }
                } catch (JSONException e) {
                    e.printStackTrace();
                }
            }
        }
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.
        getMenuInflater().inflate(R.menu.menu_main, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        // Handle action bar item clicks here. The action bar will
        // automatically handle clicks on the Home/Up button, so long
        // as you specify a parent activity in AndroidManifest.xml.
        int id = item.getItemId();

        //noinspection SimplifiableIfStatement
        if (id == R.id.action_settings) {
            Intent i = new Intent(this, SettingsActivity.class);
            startActivity(i);
            return true;
        }
        if (id == R.id.action_refresh) {
            //new getTournamentLayout().execute(getTournament("Layout"));
            new getTournamentLayout().execute(EVENT_ID, API_KEY, STEAMID_KEY);
            return true;
        }
        if (id == R.id.action_getmypicks) {
            new getTournamentPredictions().execute(EVENT_ID, API_KEY, STEAMID_KEY);
            return true;
        }

        return super.onOptionsItemSelected(item);
    }

    class getTournamentLayout extends AsyncTask<String, Void, String> {
        @Override
        protected void onPreExecute() {
            super.onPreExecute();
        }
        @Override
        protected String doInBackground(String... strings) {
            HttpURLConnection conn = null;
            InputStream stream;
            BufferedReader rdr = null;
            StringBuilder builder;

            try {
                URL url = new URL(String.format("http://api.steampowered.com/ICSGOTournaments_730/GetTournamentLayout/v1?event=%s&key=%s&steamid=76561198036610248&steamidkey=%s", strings[0], strings[1], strings[2]));
                conn = (HttpURLConnection)url.openConnection();
                conn.connect();

                stream = conn.getInputStream();
                rdr = new BufferedReader(new InputStreamReader(stream));
                builder = new StringBuilder();

                String line;
                while((line = rdr.readLine()) != null) {
                    builder.append(line);
                }
                return builder.toString();
            } catch (IOException e) {
                e.printStackTrace();
            } finally {
                if (conn != null) {
                    conn.disconnect();
                }
                try {
                    if (rdr != null) {
                        rdr.close();
                    }
                } catch (IOException e) {
                    e.printStackTrace();
                }
            }
            return null;
        }
        @Override
        protected void onPostExecute(String result) {
            super.onPostExecute(result);
            if(result != null) {
                try {
                    mSectionsPagerAdapter.clearFragments(); // temporary solution, need to implement data swapping in adapter
                    JSONObject json = new JSONObject(result).getJSONObject("result");
                    JSONArray sections = json.getJSONArray("sections");
                    JSONArray teamArray = json.getJSONArray("teams");
                    for(int i = 0; i < sections.length(); i++) {
                        JSONObject section = sections.getJSONObject(i);
                        JSONArray groups = section.getJSONArray("groups");
                        ArrayList<GameModel> games = new ArrayList<>();
                        for(int ii = 0; ii < groups.length(); ii++) {
                            JSONObject group = groups.getJSONObject(ii);
                            JSONArray teams = group.getJSONArray("teams");
                            JSONArray picks = group.getJSONArray("picks");

                            int winnerId = 0;
                            try {
                                winnerId = picks.getJSONObject(0).getJSONArray("pickids").getInt(0);
                            } catch (JSONException e) {
                                e.printStackTrace();
                            }

                            String teamA_name = "TBD";
                            String teamB_name = "TBD";
                            int teamA_id = teams.getJSONObject(0).getInt("pickid");
                            int teamB_id = teams.getJSONObject(1).getInt("pickid");

                            for(int j = 0; j < teamArray.length(); j++) {
                                JSONObject teamObj = teamArray.getJSONObject(j);
                                if(teamObj.getInt("pickid") == teams.getJSONObject(0).getInt("pickid")) {
                                    teamA_name = teamObj.getString("name");
                                } else if(teamObj.getInt("pickid") == teams.getJSONObject(1).getInt("pickid")) {
                                    teamB_name = teamObj.getString("name");
                                }
                            }

                            if(!teamA_name.equals("TBD") && !teamB_name.equals("TBD")){
                                if(teamA_id == winnerId) {
                                    teamA_name += " [WINNER]";
                                } else if(teamB_id == winnerId) {
                                    teamB_name += " [WINNER]";
                                }
                            }

                            GameModel gameModel = new GameModel();
                            Team teamA = new Team();
                            Team teamB = new Team();

                            teamA.id = teamA_id;
                            teamA.name = teamA_name;

                            teamB.id = teamB_id;
                            teamB.name = teamB_name;

                            gameModel.setTeamA(teamA);
                            gameModel.setTeamB(teamB);

                            gameModel.setMatchOver(winnerId != 0);
                            gameModel.setMatch(group.getString("name"));

                            gameModel.setSectionId(section.getInt("sectionid"));
                            gameModel.setGroupId(group.getInt("groupid"));

                            games.add(gameModel);
                        }
                        mSectionsPagerAdapter.addFragment(ExampleFragment.newInstance(section.getString("name"), games));
                    }
                    mViewPager.setAdapter(mSectionsPagerAdapter);
                    tabLayout.setupWithViewPager(mViewPager);

                    new getTournamentPredictions().execute(EVENT_ID, API_KEY, STEAMID_KEY);
                } catch (JSONException e) {
                    e.printStackTrace();
                    Toast.makeText(getApplicationContext(), "failed to parse response", Toast.LENGTH_SHORT).show();
                }
            } else {
                Toast.makeText(getApplicationContext(), "could not get a response", Toast.LENGTH_SHORT).show();
            }
        }
    }

    class postPick extends AsyncTask<String, Void, String> {
        @Override
        protected void onPreExecute() {
            super.onPreExecute();
        }
        @Override
        protected String doInBackground(String... strings) {
            try {
                URL url = new URL(String.format("http://api.steampowered.com/ICSGOTournaments_730/UploadTournamentPredictions/v1?key=%s", API_KEY));
                HttpURLConnection  conn = (HttpURLConnection)url.openConnection();
                conn.setReadTimeout(10000);
                conn.setConnectTimeout(15000);
                conn.setRequestMethod("POST");
                conn.setDoOutput(true);

                DataOutputStream dstream = new DataOutputStream(conn.getOutputStream());
                dstream.writeBytes(String.format("event=%s&steamid=%s&steamidkey=%s&sectionid=%s&groupid=%s&index=%s&pickid=%s&itemid=%s", EVENT_ID, STEAMID, STEAMID_KEY, strings[0], strings[1], strings[2], strings[3], strings[4]));
                dstream.flush();
                dstream.close();

                int responseCode = conn.getResponseCode();

                return String.valueOf(responseCode);
            } catch (IOException e) {
                e.printStackTrace();
            }
            return null;
        }
        @Override
        protected void onPostExecute(String result) {
            super.onPostExecute(result);
            Toast.makeText(getApplicationContext(), "response: "+String.valueOf(result), Toast.LENGTH_LONG).show();
        }
    }

    class getTournamentPredictions extends AsyncTask<String, Void, String> {
        @Override
        protected void onPreExecute() {
            super.onPreExecute();
        }
        @Override
        protected String doInBackground(String... strings) {
            HttpURLConnection conn = null;
            InputStream stream;
            BufferedReader rdr = null;
            StringBuilder builder;

            try {
                URL url = new URL(String.format("http://api.steampowered.com/ICSGOTournaments_730/GetTournamentPredictions/v1?event=%s&key=%s&steamid=76561198036610248&steamidkey=%s", strings[0], strings[1], strings[2]));
                conn = (HttpURLConnection)url.openConnection();
                conn.connect();

                stream = conn.getInputStream();
                rdr = new BufferedReader(new InputStreamReader(stream));
                builder = new StringBuilder();

                String line;
                while((line = rdr.readLine()) != null) {
                    builder.append(line);
                }
                return builder.toString();
            } catch (IOException e) {
                e.printStackTrace();
            } finally {
                if (conn != null) {
                    conn.disconnect();
                }
                try {
                    if (rdr != null) {
                        rdr.close();
                    }
                } catch (IOException e) {
                    e.printStackTrace();
                }
            }
            return null;
        }
        @Override
        protected void onPostExecute(String result) {
            super.onPostExecute(result);
            if(result != null) {
                try {
                    JSONObject json = new JSONObject(result).getJSONObject("result");
                    JSONArray picks = json.getJSONArray("picks");
                    myPredictions = picks;
                    setMyPredictions(picks);
                } catch (JSONException e) {
                    e.printStackTrace();
                    Toast.makeText(getApplicationContext(), "failed to parse response", Toast.LENGTH_SHORT).show();
                }
            } else {
                Toast.makeText(getApplicationContext(), "could not get a response", Toast.LENGTH_SHORT).show();
            }
        }
    }

    public static class ExampleFragment extends Fragment {

        private RecyclerView rvGames;
        private RecyclerViewAdapter adapter;

        public ExampleFragment() {

        }

        public static Fragment newInstance(String title, ArrayList<GameModel> items) {
            ExampleFragment f = new ExampleFragment();
            Bundle args = new Bundle();
            args.putString("TITLE", title);
            args.putParcelableArrayList("LIST", items);
            f.setArguments(args);
            return f;
        }

        @Override
        public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
            if(container == null) {
                return null;
            }

            LinearLayout ll = (LinearLayout)inflater.inflate(R.layout.fragment_main, container, false);
            rvGames = (RecyclerView)ll.findViewById(R.id.rvGames);

            rvGames.setLayoutManager(new LinearLayoutManager(getActivity()));

            ArrayList<GameModel> games = getArguments().getParcelableArrayList("LIST");
            adapter = new RecyclerViewAdapter(getContext(), games);

            rvGames.setAdapter(adapter);
            return ll;
        }
    }
}
